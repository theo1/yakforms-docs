Title: FAQ
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Slug: faq
Lang: fr
Nomenu: True

# Foire aux questions

## Où puis-je trouver une documentation pour Yakforms ?

​Celle-ci se trouve sur [notre site de documentation](https://docs.yakforms.org) avec :

- [un exemple d’utilisation](https://docs.yakforms.org/exemple-d-utilisation/)
- [des astuces](https://docs.yakforms.org/astuces/)
- [une explication des fonctionnalités](https://docs.yakforms.org/features/creer-un-formulaire/)
- [une explication des composants](https://docs.yakforms.org/composants/)

## J’obtiens une erreur «&nbsp;HTTP AJAX&nbsp;»

Nous rencontrons de nombreuses erreurs de ce type auxquelles nous n’avons pas de solution.

### Erreur pendant une tentative de téléchargement :

En cas d’erreur lors de la demande d’un téléchagement des résultats, il suffit de cliquer sur le lien **page d’erreur** :

![image montrant une erreur ajax](/images/forms_erreur_ajax_telechargements.png)

### Erreur pendant la saisie de l’identifiant avec qui partager un formulaire :

Il faut ignorer l’erreur en cliquant sur **OK** et en recommençant (en enlevant ou
ajoutant une lettre à l’identifiant).



## Comment partager les résultats de mon formulaire ?

Si la personne avec qui vous voulez partager les résultats de votre formulaire a un compte sur votre instance,
vous devez :

- aller dans l’onglet **Modifier**
- aller dans la section **Autres options**
- aller dans le cadre **Utilisateurs ayant accès aux résultats** pour ajouter le pseudo du compte
- cliquer sur le pseudo qui apparaît dans la liste (pour ajouter plusieurs comptes vous
  devez ajouter une virgule entre chaque)
- enregistrer en bas de la page.

Les résultats sont visibles en étant connecté et en allant **sur le formulaire**.
Le formulaire n’apparaîtra pas dans **Mes formulaires**.

**Une erreur Ajax peut survenir : c’est un bug connu mais pas encore corrigé.
Il faut l’ignorer en cliquant sur OK et en recommençant (en enlevant ou ajoutant une lettre).**



## Est-il possible de partager l’administration d’un formulaire ?

Non, [ce n’est pas possible](https://framagit.org/yakforms/yakforms/issues/9).
Seuls les **résultats** peuvent l’être (voir ci-dessus).



## Du texte indésirable apparaît sur la page publique du formulaire alors qu’il n’y est pas sur la page d’édition. Pourquoi ?

Il est fort probable que ce texte vienne d’un copier/coller depuis une page
web ou d’un traitement de texte (LibreOffice Writer, Microsoft Word…). En
mode édition, le code html qu’il contient est totalement interprété y compris
la partie masquée alors que sur la page publique les balises non autorisés
sont nettoyées rendant une partie du code visible.

Pour nettoyer le code html :

- cliquez sur le bouton HTML dans la barre d’outil
  de l’éditeur ![Image montrant l'éditeur de texte, le curseur clique sur le bouton "Edit HTML source"](/images/forms_mode_html.png)
- une fenêtre s’ouvre alors
- supprimez dans le code source tout ce qui ne correspond
  pas au texte que vous voulez publier.
- cliquez sur **update**
- vous revenez alors sur la page de votre formulaire
- cliquez sur **Enregistrer** en bas de la page



## Est-il possible de définir un quota (comme un nombre de places limité) sur un composant, comme le faisait inscriptionfacile.com ?

Il n’est pas possible de créer un système de quota (proposer 10 places, et ne plus autoriser
à sélectionner une option si il n’y a plus de place) avec Yakforms.



## Une page « Page not found / Page introuvable » apparaît quand je vais sur mon formulaire : que faire ?

Quand le titre d’un formulaire change, **son adresse aussi** : dès lors,
il faut demander la nouvelle adresse à la personne (ou au site) qui vous
a donné le lien. Si ce formulaire est le votre, vous le trouverez en cliquant
sur le bouton **Mes formulaires**.



## Je voudrais supprimer mon compte : comment faire ?

Pour supprimer votre compte vous devez :

- aller dans vos paramètres (**Mon compte** à côté du bouton **Déconnexion**)
- puis dans l’onglet **Modifier**
- puis cliquer sur **Annuler le compte**.
- puis confirmer



## Je rencontre l’erreur suivante : «&nbsp;Vous ne pouvez pas soumettre une nouvelle entrée pour le moment&nbsp;».

La plupart du temps, si vous ne trouvez que l’introduction et non le formulaire,
c’est parce qu’une date d’expiration trop proche a été donnée. Pour modifier
cette date, rendez-vous sur la page d’édition de votre formulaire, et modifiez
la date d’expiration.

Une autre possibilité, c’est que vous ayez tout simplement clos le formulaire :

1. allez sur l’onglet **Formulaire**
2. puis **Paramètres de formulaire**
3. puis vérifiez que le **Statut du formulaire** est bien **Ouvert**.



## Comment exporter les résultats pour les utiliser dans un tableur ?

Pour faire un export de vos résultats vers un tableur :

1. identifiez-vous sur Yakforms
2. allez sur votre formulaire
3. cliquez sur l’onglet **Résultats**
4. cliquez sur **Téléchargement**
5. cliquez sur le bouton **Téléchargement** en bas de page (vous pouvez modifier
   les options si besoin, mais les réglages par défaut devraient convenir)
6. enregistrez ce fichier .csv sur votre ordinateur, à l’emplacement de votre
   choix
7. ouvrez votre tableur (Excel, LibreOffice, OpenOffice ou autre)
8. ouvrez le fichier `.csv` précédemment exporté (par défaut le séparateur est
   la « tabulation »)

Les graphiques ne sont pas exportables : vous devez prendre des captures d’écrans de ceux-ci.



## Comment cloner un formulaire ?

Si vous souhaitez cloner un formulaire personnel, consultez ce formulaire en étant connecté à votre compte
et aller sur l’onglet **Partager** puis, dans la section **Clonage**,
cliquez sur le lien qui vous est donné.

Vous avez aussi la possibilité de faire figurer votre formulaire dans la
liste des modèles publics. Dans l’onglet **Modifier**, cochez la case
**Lister parmi les modèles**.

Si vous souhaitez créer un formulaire cloné à partir d’un modèle public
il suffit de cliquer sur le lien **Cloner** correspondant au modèle
dans **la liste des modèle disponibles de votre instance** (https://yakforms.example/templates).

**Si la section n’apparaît pas, veuillez désactiver votre bloqueur de publicités.**


## Comment supprimer un modèle de formulaire ?

Connectez-vous sur votre compte personnel, allez sur le formulaire en question,
cliquez sur **Modifier** et décochez la case **Lister parmi les modèles**.



## Pourquoi la mise en forme ne s’applique pas sur la page publique de mon formulaire ?

Suite à la découverte d’une faille de sécurité (heureusement non exploitée)
nous avons dû limiter les possibilités d’édition, ce qui explique la disparition
du formatage.

Nous espérons pouvoir rétablir certaines fonctionnalités de mise en page
dès que nous en aurons le temps.



## Comment puis-je imprimer les résultats ou les questions d’un questionnaire ?

Le logiciel que nous utilisons pour Yakforms ne permet pas d’imprimer
les résultats autrement que par le fichier texte téléchargeable. La seule
solution pour imprimer les graphiques est de faire des captures d’écrans
de vos résultats.

Il n’est pas possible non plus d’imprimer les questions pour en faire un questionnaire papier
autrement qu’en utilisant la fonction impression de votre navigateur.



## Mes résultats s’affichent avec des caractères spéciaux à la place des accents dans les imports : comment régler cela ?

Ceci est dû à un problème d’encodage. Pour LibreOffice \_ vous pouvez sélectionner
le format UTF8 directement sur la fenêtre quand vous ouvrez un fichier `.csv`
ou `.tsv`.

Pour Excel, vous devez lui préciser qu’il faut utiliser UTF-8. N’ayant pas
Excel, mais en regardant sur le web, il semblerait qu’il faille ouvrir Excel,
puis faire **Fichier** > **Ouvrir**, puis aller chercher le fichier
pour l’importer dans Excel et lui préciser ensuite qu’il faut utiliser UTF-8
au niveau de **Origine du fichier**.

Si vous version d’Excel ne propose pas l’encodage UTF-8 vous pouvez afficher
les résultats en mode tableau dans Yakforms (ainsi les accents sont préservés),
sélectionner et copier tous les résultats puis les coller dans une feuille
vierge d’Excel (ou d’un autre tableur) via **fichier** > **collage
spéciale** > **texte unicode**.



## Mon questionnaire expire bientôt ! Comment modifier sa date d’expiration ?

Pour modifier votre formulaire, vous devez :

1. aller dans **Modifier**
2. cliquer sur **Autres options**
3. modifier la date dans l’encadré **Date d’expiration** (**La date d’expiration ne
   peut pas excéder 6 mois suivant votre dernière mise à jour du formulaire**)
4. cliquer sur **Enregistrer** (tout en bas)



## Comment identifier les utilisateurs et utilisatrices qui répondent à mon questionnaire et ne plus avoir «&nbsp;Anonymous&nbsp;» dans les résultats ?

Par défaut la colonne **Utilisateur** dans l’onglet **Résultats** > **Participations**
indique **Anonymous**. Pour changer cela il faut que les répondant⋅es se créé⋅es un compte
Yakforms et répondent aux formulaires en étant connecté⋅e⋅s

Une autre façon d’identifier les répondant⋅e⋅s est de faire un champ **Nom** ou autres
que les personnes interrogées devront compléter.
L’identification sera visible dans **Résultats** > **Tableau**.

![Image montrant le tableau de résultat, avec un champ "Qui-êtes-vous"](/images/forms_resultats_tableau.png)



## Je voudrais une adresse de formulaire plus simple. Comment faire ?

Pour avoir une adresse plus simple à retenir ou plus courte vous pouvez utilisez un raccourcisseur d’url
comme [huit.re](https://huit.re) (sans compte, donc impossible
de supprimer ou modifier la redirection).



## Est-il possible d’avoir l’interface de Yakforms dans une autre langue que le français ?

Pour le moment, ce n’est pas possible. C’est [une fonctionnalité
souhaitée](https://framagit.org/yakforms/yakforms/-/issues/34) mais nous ne savons
pas quand elle sera disponible.

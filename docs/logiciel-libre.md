# Logiciel libre

Yakforms est basé sur le CMS [Drupal 7](https://www.drupal.org/), et en particulier sur les modules [Webform](https://www.drupal.org/project/webform/) et [Form builder](https://www.drupal.org/project/form_builder/). Comme ces derniers, Yakforms est entièrement distribué sous license [AGPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html).

## Anonymat

Les données issues de Yakforms sont anonymisées, il est impossible pour un créateur de formulaire de récupérer les adresses IP des répondants. Il va de soi cependant que si le créateur du formulaire place des champs « Nom » ou « Prénoms » et que les répondant⋅e les rempli avec des données correctes, ces données pourront être récupérées (c'est pourquoi il ne faut __JAMAIS__ saisir un mot de passe dans un formulaire Yakforms !).

## Autonomie

Yakforms autorise et permet [une installation du service sur vos propres serveurs](https://framagit.org/yakforms/yakforms/-/wikis/Installing-Yakforms-through-the-installation-profile), afin de garantir un contrôle total sur les données de vos répondant⋅es.

## Vie privée garantie

Yakforms vise à luter contre les atteintes à la vie privée notamment lorsqu'il s'agit de collectes massives telles que pratiquées par GAFAM (Google, Apple, Facebook, Amazon, Microsoft). Cependant, la sécurité de vos données dépend intégralement de la structure qui héberge le site Yakforms que vous utilisez. Pour plus d'informations, consultez les Conditions Générales d'Utilisation (CGU) de votre fournisseur de service.

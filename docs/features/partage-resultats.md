
# Partage des résultats

## Avec un autre compte

Vous pouvez partager les résultats (**pas l'administration**) avec un compte Framforms. Pour cela vous devez :

1. vous rendre dans l'onglet **Modifier**
2. puis **Autres options**
3. puis **Utilisateurs ayant accès aux résultats**
4. entrer le nom du compte avec lequel vous souhaitez partager les résultats
5. cliquer sur **Enregistrer** tout en bas

Il vous reste à donner le lien de votre formulaire au compte : celui-ci verra alors l'onglet **Résultats** sur cette page.

## Résultats publics

Il est possible de partager les résultats publiquement (sans nécessité d'avoir un compte pour les voir).

<p class="alert alert-warning">ATTENTION : En choisissant "Oui", l'ensemble des résultats de votre formulaire sera accessible <b>à tout le monde</b>. Même si les personnes ne peuvent pas modifier les résultats, cela n'est <b>PAS RECOMMANDÉ</b> car vous pouvez rendre public des informations personnelles (type : nom, prénom, email, adresses ou n° de téléphone) de personnes qui ne souhaitaient pas les rendre publiques.</p>

Pour cela vous devez :
1. vous rendre dans l'onglet **Modifier**
2. puis **Autres options**
3. puis **Accès public aux résultats**
4. sélectionner `Oui`
5. cliquer sur **Enregistrer** tout en bas

Il vous reste à donner le lien de votre formulaire : sera alors affiché l'onglet **Résultats publics** sur cette page.

# Exporter les données Yakforms en mode publipostage

_Auteur : pyg, pour Framasoft – Licence CC by-sa_

**Objectif** : obtenir un document où chaque participation fait l’objet d’une page séparée
et mise en forme.

**Prérequis** : un formulaire Yakforms avec des participations ; LibreOffice (ça marche sans doute
aussi avec Word et Excel mais je ne peux pas indiquer la manipulation)

## Situation de départ

Vous avez un Yakforms de ce type, par exemple.

![image](../images/publipostage1.jpg)

## Étape 1 – Exporter les données

Cliquez sur `Résultats`, puis `Téléchargement` et sélectionnez « `Texte délimité (.csv)` » (laissez les autres options par défaut). Cliquez sur `Téléchargement`.

![image](../images/publipostage2.jpg)

<div class="alert-info alert">
NB : si le téléchargement génère une erreur, pas de panique, cliquer sur le lien « page d’erreur » devrait régler le problème.
</div>

## Étape 2 – transformer le fichier en tableur

Suite à l’étape précédente, vous devez avoir téléchargé un fichier .csv ou .tsv sur votre ordinateur.

Maintenant, ouvrez LibreOffice Calc, et faites `Fichier` > `Ouvrir` (et sélectionnez votre fichier). Une fenêtre apparaît.

![image](../images/publipostage3.jpg)

Logiquement, l’aperçu devrait être OK. Vérifiez que les accents passent bien, si ce n’est pas le cas, sélectionnez bien `Jeu de caractères` > `UTF-8`. Cliquez sur `OK`.

![image](../images/publipostage4.jpg)

Votre fichier est maintenant dans Calc ! :)

Maintenant, supprimez les 2 premières lignes, qui sinon vont nous gêner. (sélectionnez les deux premières lignes en cliquant sur leurs numéros, puis clic-droit, puis `supprimer les lignes`)

![image](../images/publipostage5.jpg)

Dernière étape : enregistrez ce fichier au format LibreOffice Calc dans un dossier de votre choix.

![image](../images/publipostage6.jpg)

Vous pouvez quittez LibreOffice Calc.

## Étape 3 – Préparation du document d’export

Ouvrez LibreOffice Writer. Préparez un document libre qui servira de modèle pour le document de sortie. Vous pouvez le mettre en forme selon vos choix (gras, couleurs, titres, etc).

Ci-dessous, un exemple de modèle de document de sortie.

![image](../images/publipostage7.jpg)

## Étape 4 – Lier ce document à notre base de données de réponses.

C’est la partie la plus complexe (car la moins connue du public).

Dans le menu `Insertion`, choisissez `Champs` puis `Autres champs…`

![image](../images/publipostage8.jpg)

Cela ouvrira une fenêtre `champs`, dans laquelle vous pourrez choisir l’onglet `Base de données`.
Cliquez sur `Parcourir`, et sélectionnez le fichier Calc (.ods) que vous avez créé à l’étape 2.

![image](../images/publipostage9.jpg)

Une nouvelle base de données s’affiche alors dans la fenêtre Champs, et vous devriez voir vos champs de réponses s’afficher (ne double-cliquez pas sur les champs pour le moment).

![image](../images/publipostage10.jpg)

## Étape 5 – Insérez les champs souhaités

C’est la partie sympa ;)

A partir de la fenêtre Champs, lorsque vous double-cliquez sur l’un des champs, cela l’insère dans votre modèle. Vous pouvez positionner votre curseur sur le modèle à l’endroit désiré, puis depuis la fenêtre `Champs`, double cliquez pour insérer le champ souhaité.

![image](../images/publipostage11.jpg)

Répétez cette opération autant de fois que nécessaire, puis cliquez sur fermer.

![image](../images/publipostage12.jpg)

Voilà, par exemple, à quoi ressemble mon modèle avec les champs insérés.

## Étape 6 – Générer le(s) document(s)

C’est la partie la plus simple !

Cliquez sur `Fichier` > `Imprimer` (comme si vous souhaitiez imprimer le document). Une fenêtre apparaît, répondez « Oui ».

![image](../images/publipostage13.jpg)

Dans la fenêtre suivante Mailing qui s’ouvre alors, choisissez `Sortie` > `Fichier`. Vous pouvez modifier les options qui vous intéressent. Par exemple enregistrer la sortie dans un seul document, ou dans autant de documents qu’il y a de participations. Cliquez sur `OK`.

![image](../images/publipostage14.jpg)

Enregistrez alors le document généré au nom et format de votre choix.

Ouvrez ce nouveau document, et voilà !

![image](../images/publipostage15.jpg)

Vous pouvez évidemment imprimer ce document, l’exporter en PDF, etc.

Grâce à votre document modèle (que je vous conseille d’enregistrer), vous pouvez facilement faire des adaptations de formes, et réexporter en répétant l’étape 6.

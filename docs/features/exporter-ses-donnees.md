# Exporter ses données

Afin de passer d'une instance Yakforms à une autre, il est possible d'exporter ses formulaires un par un, et de les réimporter dans un autre site (**si l'administrateur⋅ice de celui-ci le permet**).

## Exporter la structure du formulaire

Vous pouvez exporter vos formulaires pour les importer dans une autre instance utilisant le logiciel utilisé par Yakforms ([drupal](https://www.drupal.org/) avec le module [webform](https://www.drupal.org/project/webform)). Pour ce faire vous devez :

* cliquer sur l'onglet **Formulaire**
* cliquer sur **Exporter**

La structure du formulaire et les conditions seront exportées.

## Exporter les soumissions

Il est également possible d'exporter les réponses d'un formulaires (soumissions).

Pour exporter les soumissions de votre formulaire vous devez :

* cliquer sur l'onglet **Résultats**
* cliquer sur **Téléchargement**
* **Facultatif** : modifier les préférences de téléchargement des résultats
* cliquer sur le bouton **Téléchargement**

**Il n'est pas possible d'importer des réponses dans un formulaire**.

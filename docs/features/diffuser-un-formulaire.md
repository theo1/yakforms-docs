## Participation facile

Les formulaires Yakforms s'adaptent automatiquement à votre support et peuvent être visualisés et remplis sur mobiles et tablettes.

![mobile](../images/fonctionnalites_forms_mobile.png)

## Diffusion et intégration simplifiées

Vous pouvez facilement partagez vos fomulaires par email, sur un site web, ou sur les réseaux sociaux. Vous pouvez même les embarquer sur votre site web.

Vous trouverez l'onglet "Partager" en haut de votre formulaire.

**Plusieurs possibilités s'offrent à vous** :

* en copiant/collant l'adresse URL (dans un email ou sur votre site Web, par exemple),vous permettez aux personnes ciblées d'accéder au formulaire en un clic! Vous pouvez de la même manière le partager sur les réseaux sociaux.
* Yakforms vous fourni directement le code pour embarquer le formulaire, c'est-à-dire l'insérer dans un autre site Web (iframe). Pour ce faire, vous devez :
  * cliquer sur l'onglet **Partager** de votre formulaire
  * copier le code dans la section **Embarquer le code**
  * coller ce code dans votre site
    <p class="alert-warning alert">A noter que tous les sites ne permettent pas d'inclure des iframes dans leurs contenus. Il vous faudra peut-être disposer d'autorisations ou de plugins spécifiques ( [exemple pour Wordpress](https://wordpress.org/plugins/iframe/) ).
    </p>


## Formulaire de contact personnel

Pour vous contacter directement en cas de question sur votre formulaire, les utilisateur⋅ices ont accès à votre formulaire de contact personnel en bas de vos formulaires. Votre mail n'est pas affiché directement sur Yakforms, mais le message vous sera transmis par mail.

![contacter l'auteur](../images/fonctionnalites_formulaire_contact.png)

Si jamais vous ne souhaitez pas laisser la possibilité aux utilisateur⋅ices de vous contacter, il est possible de désactiver ce formulaire en suivant les étapes suivantes :

1. vous rendre sur **Mon compte**, puis **Modifier**
2. décochez la case **Formulaire de contact personnel**.

<p class="alert alert-warning">
Une forte utilisation d'un site Yakforms peut engendrer une grande pression de support. Les utilisateur⋅ices essayant de contacter l'auteur⋅rice d'un formulaire contactent régulièrement par erreur l'administrateur⋅ice de l'instance. Dans la mesure du possible, afin de faciliter le contact direct entre utilisateur⋅ices et auteur⋅ices, il est conseillé de laisser ce formulaire activé.
</p>

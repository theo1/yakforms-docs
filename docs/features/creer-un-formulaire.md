# Création rapide de formulaires

## Glissez-déposez, pour plus de facilité !

Pour créer votre formulaire, aucune compétence technique particulière n'est nécessaire : il vous suffit de glisser-déposer les champs souhaités.

![glisser/déposer les composants]( ../images/fonctionnalites_forms_creation_rapide.png)

## Création simple

Lorsque vous vous connectez sur votre compte Yakforms, la page d'accueil vous permet d'accéder rapidement à la création d'un nouveau formulaire.

![création simple]( ../images/fonctionnalites_fomrs_creation_simple.gif)

## Création à partir d'un modèle

Vous pouvez également utiliser un modèle existant en furetant dans la rubrique « Créer un formulaire depuis un modèle ». Les modèles sont proposés par les autres utilisateurs et sont d'une grande diversité : n'hésitez pas à les cloner et les enregistrer pour y accéder, et choisir en fonction de vos propres besoins !
![création d'un formulaire à partir d'un modèle]( ../images/fonctionnalites-forms-clone.gif)

## Formulaire sur plusieurs pages

Votre formulaire est long… Trop long ? Vous pouvez le découper sur plusieurs pages afin d'en faciliter le remplissage, en glissant-déposant le champ "saut de page" aux endroits souhaités.

![multipages](../images/fonctionnalites_forms_multipages.png)

## Multiple choix de champs

Que vous ayez besoin de champs textes, de cases à cocher, d'une liste de sélection, de donner la possibilité à vos utilisateurs de mettre en ligne des fichiers, etc... Yakforms vous propose de nombreux choix de champs qui vous permettront de créer un formulaire adapté à vos besoins.

![multiple choix de champs](../images/fonctionnalites_forms_champs.png)

## Multiples thèmes

Par défaut, le thème est plutôt neutre, mais vous pourrez bientôt en choisir d'autres (en cours), plus adaptés ou adaptables - dans les grandes lignes - à votre charte graphique.

![Theme responsive standard](../images/fonctionnalites_forms_theme1.png)

![theme Frama](../images/fonctionnalites_forms_theme2.png)

Nous avons encore un peu de travail à fournir pour vous proposer un plus grand choix de thèmes. Mais nous ne vous avons pas oublié !

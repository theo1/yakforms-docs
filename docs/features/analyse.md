
# Analyse et téléchargement

## Visualisation

Vous pouvez facilement accéder à la liste des soumissions, et visualiser ces dernières les unes à la suite des autres.

![Visualisation](../images/fonctionnalites_forms_visualisation.png)

## Téléchargement

Les soumissions peuvent être téléchargées aux formats tableurs habituels (``.csv`` pour Libre Office Calc ou Excel) en vue de traitements plus complexes.

![téléchargement](../images/fonctionnalites_forms_telechargement.png)

Pour récupérer vos résultats :

1. cliquez sur **Résultats** puis **Téléchargement**
2. (optionnel) vous pouvez sélectionner des options, mais la sélection par défaut couvre la plupart des besoin
3. cliquez sur **Téléchargement** (tout en bas)
4. ouvrez le fichier avec LibreOffice Calc (ou excel) ou enregistrez-le sur votre ordinateur

![résultats dans LibreOffice Calc](../images/fonctionnalites_forms_telechargement_tsv.png)

## Analyse

Un outil d'analyse des réponses (très basique) est inclus, vous permettant d'obtenir immédiatement des statistiques. Par exemple, le nombre de réponses sur un champ texte, le total ou la moyenne sur les champs numériques, ou le détail des réponses sur les champs à choix multiples.

![analyse](../images/fonctionnalites_forms_analyse.png)

## Graphes

Les réponses analysées peuvent faire l'objet d'un graphique automatiquement généré. (NB : pour des graphiques plus complets, téléchargez vos résultats dans un tableur comme Libre Office)

![graphes](../images/fonctionnalites_forms_graphes.png)
